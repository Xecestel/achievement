################################################################################
# ACHIEVEMENT SCENE FOR GODOT ENGINE 3
# Version 1.0
# © Xecestel 2020
# MIT LICENSE
################################################################################
#
# MIT License
#
# Copyright © 2020 Celeste Privitera
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
################################################################################

extends Control

# Variables

export (String, FILE, "*.png") var default_achievement_icon = ""
export (String, FILE) var unlock_sound_path = ""
export (String) var achievement_unlocked_text = "Achievement unlocked!"
export (bool) var caps_lock = false
export (float, EXP, 0.001, 4096, 0.001) var wait_time = 1.0

var unlock_sound : Resource = null
var shown : bool = false

onready var icon_rect = get_node("AchievementPopup/AchievementIcon")
onready var achievement_unlocked_label = get_node("AchievementPopup/Labels/AchievementUnlocked")
onready var achievement_name_label = get_node("AchievementPopup/Labels/AchievementName")
onready var se = get_node("UnlockSound")
onready var animation = get_node("AnimationPlayer")
onready var timer = get_node("Timer")

##########

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	achievement_unlocked_label.text = achievement_unlocked_text
	achievement_unlocked_label.uppercase = caps_lock
	achievement_name_label.uppercase = caps_lock
	icon_rect.texture = load(default_achievement_icon)
	unlock_sound = load(unlock_sound_path)
	self.visible = false


func show_achievement(achievement : Dictionary = {}) -> void:
	self.visible = true
	if achievement and not achievement.empty():
		set_achievement(achievement)
	animation.play("pop_up")
	shown = true


func set_achievement(achievement : Dictionary) -> void:
	if achievement.has("icon"):
		icon_rect.texture = achievement["icon"]
	if achievement.has("name"):
		achievement_name_label.text = achievement["name"]


func play_sound(sound : Resource = null) -> void:
	se.stream = unlock_sound if sound == null else sound
	se.play()


func _on_Timer_timeout() -> void:
	animation.play_backwards("pop_up")
	shown = false


func _on_AnimationPlayer_animation_finished(anim_name : String) -> void:
	if shown:
		play_sound()
		timer.start(wait_time)
	else:
		visible = false
