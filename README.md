# Achievement
## Version 1.0
## © Xecestel
## MIT License

## Overview
Made with Godot Engine 3.2.
This scene allows the user to pop-up a simple Achievement node on the screen whenever an achievement is unlocked by the player. It can be set to show the name of the achievement, an icon for it and even to play a sound effect.  
The "Achievement unlock" text can be edited too.  
  
## Configuration
Just place it on your scene and call it whenever you need it. Use the exported variables to customize it from the editor however you like.  
  
## Methods 
<table>
	<tr>
		<td>Method</td>
		<td>Description</td>
	</tr>
	<tr>
		<td><code>func set_achievement(achievement : Dictionary) -> void:</code></td>
		<td>This allows you to set the next achievement you want to pop-up. It expects a dictionary with an <code>"icon"</code> and a <code>"name"</code> key. If it doesn't find any of these keys it will just show an empty text and/or icon.</td>
	</tr>
	<tr>
		<td><code>func show_achievement(achievement : Dictionary = {}) -> void</code></td>
		<td>This makes the module pop-up an achievement. To set the correct achievement you can either use the <code>show_achievement</code> argument or just use the default setter.</td>
	</tr>
	<tr>
		<td><code>func play_sound(sound : Resource = null) -> void:</code></td>
		<td>You can use this to play the sound effect. It can be useful if you don't want to use the default animation for any reason. If you don't use the argument, it will just play the default one.</td>
	</tr>
</table>  
  
## Credits
Designed and programmed by Celeste Privitera (@xecestel).  
  
## Licenses
Achievement  
Copyright © 2020 Celeste Privitera  
This software is an open source project licensed under MIT License. Check the LICENSE file for more info.  
  
## Changelog
  
### Version 1.0
- Initial Commit
